import numpy as np
from copy import deepcopy
HIDNUM = 10

class QNET(object):
    def __init__(self):
        self.tc = 0
        # weights from state to hidden units
        self.W = 0.01*np.random.randn(9,HIDNUM)
        # biases of hidden units
        self.b  = np.zeros(HIDNUM)
        self.U = 0.01*np.random.randn(HIDNUM,9)
        self.c  = np.zeros(9)
        self.random = False
        self.gamma = 0.0
        self.lr    = 0.1
    def move(self,board):
        print("QNET Move")
        if board.won() or board.tied():
            return None
        if self.random: # Qnet is not used, make random move
            while True:
                x = np.random.randint(3)
                y = np.random.randint(3)
                for (x,y) in board.fields:
                    #print(board.fields[x,y])
                    if board.fields[x,y]==".":
                        return x,y
        else: # Qnet is used
            # get state of board and present it in a vector
            self.state    = get_state(board)
            self.qs,self.h  = self.infer(self.state)
            #print(self.qs)
            x,y,self.qinx = take_action(self.qs,board)
            return x,y
                
    def infer(self,state):
        # infer hidden units
        h = np.matmul(state,self.W) + self.b
        h = np.tanh(h)
        # infer q function
        q = np.matmul(h,self.U) + self.c
        return q,h
    
    def update(self,board):
        # assess the situation
        print("assess situation")
        reward,bestnextq = self.assess(board)
        err = (reward + self.gamma*bestnextq-self.qs[0,self.qinx])
        print((err*err,self.qs[0,self.qinx]))
        h = self.h[0,:]
        self.c[self.qinx] += self.lr*err
        dh = err*self.U[:,self.qinx]
        self.U[:,self.qinx] += self.lr*err*h
        dh*=(1-np.square(h))
        self.b += self.lr*dh
        self.W+= self.lr*np.matmul(np.transpose(self.state),dh[np.newaxis,:])
        
    def assess(self,board,reward_factor = 2):
        predict, prob, vec = best_opponent_move(board)
        #print((predict,prob))
        if predict==0: # absolutely win
            reward = 1 # *prob*reward_factor
            bestnextq = 0
            print("qnet is absolutely going to win, reward = %f"%reward)
        elif predict==1: # absolutely lose
            reward = -1  #*prob*reward_factor
            bestnextq = 0
            print("qnet is absolutely going to lose, reward = %f"%reward)
        elif predict==2: # absolutely tie
            reward = 0.5 # 0.5*prob*reward_factor
            bestnextq = 0
            print("qnet is absolutely going to tie, reward = %f"%reward)
        elif predict==3: # likely to lose
            reward = -1  #-1*prob*reward_factor
            bestnextq = 0
            print("qnet is likely  to lose, reward = %f"%reward)
        elif predict==4: # likely to tie
            reward = 0.25 #*prob*reward_factor
            bestnextq = 0
            print("qnet is likely to tie, reward = %f"%reward)
        else: # undicided
            reward = 0.1#*reward_factor
            bestnextq  = self.allq(vec)
            print("still playing ... good, reward = %f"%reward)
            
        return reward,bestnextq

    def allq(self,vec):
        qd,_ = self.infer(vec)
        mx = None
        for i in range(vec.shape[1]):
            v = vec[0,i]
            q = qd[0,i]
            if v == 0:
                if mx is None:
                    mx = q
                else:
                    if mx<q:
                        mx = q
        return mx
        
def best_opponent_move(board):
    winning = board.won() # assess right after qnet's move
    if winning: # qnet's absolutely won
        #print("must be O")
        #print(board.fields[winning[0]])
        return 0,1,None
    if board.tied(): # qnet's absolutly tied
        return 2,1,None
    
    pboard = deepcopy(board)
    num_move = 0
    for (x,y) in board.fields:
        if pboard.fields[x,y] == ".":
            num_move+=1
            
    won_count = 0
    tie_count = 0
    for (x,y) in pboard.fields:
        if pboard.fields[x,y]==".":
            mx = x
            my = y
            pboard.fields[x,y]="X"
            (pboard.player1,pboard.player2) = (pboard.player2,pboard.player1)
            if pboard.won():
                won_count +=1
            elif pboard.tied():
                tie_count +=1
            pboard.fields[x,y]="."
            (pboard.player1,pboard.player2) = (pboard.player2,pboard.player1)
        
    #print((won_count,tie_count,num_move))
    if won_count == num_move: # qnet will absolutely lose
        return 1,1,None
    if won_count>0: # qnet will be likely to lose
        return 3,1.0*won_count/num_move,None
    if tie_count == num_move: # qnet will  absolutely tie
        return 2,1,None
    if tie_count>0: # qnet will likely to  tie
        return 4,1.0*tie_count/num_move,None

    # Unknown
    pboard.fields[mx,my] = "X"
    return 5, None, get_state(pboard)
    
            
def get_state(board):
    vec = np.zeros((1,9))
    i = 0
    for (x,y) in board.fields:
        if board.fields[x,y]==".":
            vec[0,i] = 0
        elif board.fields[x,y]=="X":
            vec[0,i] = 1
        if board.fields[x,y]=="O":
            vec[0,i] = -1
        i+=1
    return vec

def take_action(qs,board):
    i = 0
    maxq = 0
    for (x,y) in board.fields:
        if board.fields[x,y]==".":
            q = qs[0,i]
            if maxq == 0 or q> maxq:
                qinx = i
                maxq = q
                mx = x
                my = y
        i+=1
    return mx,my,qinx
